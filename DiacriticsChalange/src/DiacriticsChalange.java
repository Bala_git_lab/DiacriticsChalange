import java.io.BufferedWriter;
import java.io.CharArrayReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class DiacriticsChalange {
	
	
	public static ArrayList<File> fisiere;
	
	
	public static HashMap<String, HashMap<String, HashMap<String, Cuvant>>> map;
	
	//3 GRAM
	public static HashMap<String, HashMap<String, Cuvant>> inside_map1;
	public static HashMap<String, Cuvant> inside_map2;
	
	//1 GRAM 
	public static HashMap<String, HashMap<String, Cuvant>> map1;
	public static HashMap<String, Cuvant> inside_map;
	
	
	public static void main(String[] _args) throws IOException
	{
		fisiere = new ArrayList<>();
		
		File file1 = new File("/home/bala/Desktop/DiacriticsChalange/diacritics-restoration-corpus/antena3_rdia_v7");
		fisiere.add(file1);
		File file2 = new File("/home/bala/Desktop/DiacriticsChalange/diacritics-restoration-corpus/antena3_wdia_v7");
		fisiere.add(file2);
		File file3 = new File("/home/bala/Desktop/DiacriticsChalange/diacritics-restoration-corpus/europarl-v6.ro-en.ro_v7");
		fisiere.add(file3);
		File file4 = new File("/home/bala/Desktop/DiacriticsChalange/diacritics-restoration-corpus/libertatea_rdia_v7");
		fisiere.add(file4);
		File file5 = new File("/home/bala/Desktop/DiacriticsChalange/diacritics-restoration-corpus/libertatea_wdia_v7");
		fisiere.add(file5);
		File file6 = new File("/home/bala/Desktop/DiacriticsChalange/diacritics-restoration-corpus/misc_v7");
		fisiere.add(file6);
		File file7 = new File("/home/bala/Desktop/DiacriticsChalange/diacritics-restoration-corpus/realitatea_rdia_v7");
		fisiere.add(file7);
		File file8 = new File("/home/bala/Desktop/DiacriticsChalange/diacritics-restoration-corpus/realitatea_wdia_v7");
		fisiere.add(file8);
		File file9 = new File("/home/bala/Desktop/DiacriticsChalange/diacritics-restoration-corpus/talkshows.train_v7");
		fisiere.add(file9);
		
		
		
		
		
		map = new HashMap<>();
		inside_map1 = new HashMap<>();
		inside_map2 = new HashMap<>();
		map1 = new HashMap<>();  
		
		
		
		String secventa_cu_diacritice = "";
		
		//ANTRENARE 1 CUVANT
		for (int k1 = 0 ; k1 < fisiere.size(); k1++)
		{
			
			System.out.println(k1);
			Scanner scan = new Scanner(fisiere.get(k1));
            String linie;
            String cuvinte[];
            secventa_cu_diacritice = "";
            String secventa_fara_diacritice = "";
        
            int nr_linie=0;
            while (scan.hasNextLine())
            {
                nr_linie++;
                linie = scan.nextLine();
                //System.out.println(linie);
                cuvinte = linie.split("\\s+");
                for (int i = 0; i < cuvinte.length ; i++)
                {
                    secventa_cu_diacritice = "";
                    secventa_fara_diacritice = "";
                    secventa_cu_diacritice = cuvinte[i];
                    //System.out.println(secventa_cu_diacritice);
                    char[] secventa = secventa_cu_diacritice.toCharArray();
                    for (int k = 0; k < secventa_cu_diacritice.length(); k++)
                    {
                        switch(secventa[k])
                        {
                            case 'î':
                                secventa[k] = 'i';
                                break;
                            case 'â':
                                secventa[k] = 'a';
                                break;
                            case 'ă':
                                secventa[k] = 'a';
                                break;
                            case 'ţ':
                                secventa[k] = 't';
                                break;
                            case 'ş':
                                secventa[k] = 's';
                                break;
                        }
                    }
                    secventa_fara_diacritice = String.valueOf(secventa);
                    //System.out.println(secventa_fara_diacritice);
            
            
                if (map1.isEmpty() == true)
                {
                    inside_map = new HashMap<>();
                    Cuvant date_secventa = new Cuvant(1);
                    inside_map.put(secventa_cu_diacritice, date_secventa);
                    map1.put(secventa_fara_diacritice,inside_map);
                }
                else
                {
                    if(map1.containsKey(secventa_fara_diacritice))
                    {
                        if(map1.get(secventa_fara_diacritice).containsKey(secventa_cu_diacritice))
                        {
                            inside_map = map1.get(secventa_fara_diacritice);
                            inside_map.replace(secventa_cu_diacritice,new Cuvant(inside_map.get(secventa_cu_diacritice).nr_aparitii + 1));
                            map1.replace(secventa_fara_diacritice, inside_map);
                        }
                        else
                        {
                            inside_map = map1.get(secventa_fara_diacritice);
                            inside_map.put(secventa_cu_diacritice, new Cuvant(1));
                            map1.replace(secventa_fara_diacritice, inside_map);
                        }
                    }
                    else
                    {
                        inside_map = new HashMap<>();
                        Cuvant date_secventa = new Cuvant(1);
                        inside_map.put(secventa_cu_diacritice, date_secventa);
                        map1.put(secventa_fara_diacritice,inside_map);
                    }
                }
            }
            
        }
		}

		
/*		
		
            	//iterez prin ele si printez hashmap-ul


    	
    	for (Map.Entry<String, HashMap<String, Cuvant>> entry1 : map1.entrySet()) {
    			String cuvant_fara_diacritice = entry1.getKey();
    				
    			for (Map.Entry<String, Cuvant> entry2 : entry1.getValue().entrySet()) {
    				String cuvant_cu_diacritice = entry2.getKey();
    				Cuvant date_cuvant = entry2.getValue();
    				System.out.println(cuvant_fara_diacritice + ": " + cuvant_cu_diacritice + ": " + date_cuvant.nr_aparitii);
    				// ...
    				}
    	}
    		
	*/	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//ANTRENARE 3 GRAM
    	for (int k = 0; k < fisiere.size(); k++)
    	{
    	System.out.println(k);
    	String linie;
    	String[] cuvinte;
        Scanner scan = new Scanner(fisiere.get(k));
		while (scan.hasNextLine())
		{
			
			linie = scan.nextLine();
			cuvinte = linie.split(" ");
			String secventa;
			String cuvant_fara_diacritice;
			
			
			
			secventa = cuvinte[0] + " " + cuvinte[1];
			cuvant_fara_diacritice = secventaFaraDiacritice(cuvinte[2]);
			
			if(map.isEmpty())
			{
				inside_map1 = new HashMap<>();
				inside_map2 = new HashMap<>();
				
				//mapez cuvantul cu diacritice la nr_de apartii
				Cuvant date_cuvant = new Cuvant(1);
				inside_map2.put(cuvinte[2], date_cuvant);
				
				//mapez cuvantul cu fara diacritice la hashmapul care mapeaza forma cu diacritice la nr de aparitii
				inside_map1.put(cuvant_fara_diacritice, inside_map2);
				
				//mapez secventa de doua cuvinte cu diacritice la hashmapul care mapeaza cuvantul fara diacritice la hashmapul care mapeaza formele cu diacritice la nr de apariitii
				map.put(secventa, inside_map1);
			}
			else
			{
				if(map.containsKey(secventa) == false)
				{
					inside_map1 = new HashMap<>();
					inside_map2 = new HashMap<>();
					
					//mapez cuvantul cu diacritice la nr_de apartii
					Cuvant date_cuvant = new Cuvant(1);
					inside_map2.put(cuvinte[2], date_cuvant);
					
					//mapez cuvantul cu fara diacritice la hashmapul care mapeaza forma cu diacritice la nr de aparitii
					inside_map1.put(cuvant_fara_diacritice, inside_map2);
					
					//mapez secventa de doua cuvinte cu diacritice la hashmapul care mapeaza cuvantul fara diacritice la hashmapul care mapeaza formele cu diacritice la nr de apariitii
					map.put(secventa, inside_map1);
				}
				else // contine secventa cu diacritice
				{
					if(map.get(secventa).containsKey(cuvant_fara_diacritice) == false)//nu a mai intalnit niciodata nicio forma a acelui cuvant
					{
						inside_map2 = new HashMap<>();
						//mapez cuvantul cu diacritice la nr_de apartii
						Cuvant date_cuvant = new Cuvant(1);
						inside_map2.put(cuvinte[2], date_cuvant);
						
						//mapez cuvantul cu fara diacritice la hashmapul care mapeaza forma cu diacritice la nr de aparitii
						inside_map1 = (HashMap<String, HashMap<String, Cuvant>>) map.get(secventa);
						inside_map1.put(cuvant_fara_diacritice, inside_map2);
						
						//mapez secventa de doua cuvinte cu diacritice la hashmapul care mapeaza cuvantul fara diacritice la hashmapul care mapeaza formele cu diacritice la nr de apariitii
						map.replace(secventa, inside_map1);
					}
					
					//AICI AM RAMAS IERI
					
					else//s-au mai intalnit forme ale acestui cuvant
					{
						if(map.get(secventa).get(cuvant_fara_diacritice).containsKey(cuvinte[2]) == false)//daca nu contine forma cu diacritice
						{
							//mapez cuvantul cu diacritice la nr_de apartii
							Cuvant date_cuvant = new Cuvant(1);
							inside_map2 = map.get(secventa).get(cuvant_fara_diacritice);
							inside_map2.put(cuvinte[2], date_cuvant);
							
							//mapez cuvantul cu fara diacritice la hashmapul care mapeaza forma cu diacritice la nr de aparitii
							inside_map1 = map.get(secventa);
							inside_map1.replace(cuvant_fara_diacritice, inside_map2);
							
							//mapez secventa de doua cuvinte cu diacritice la hashmapul care mapeaza cuvantul fara diacritice la hashmapul care mapeaza formele cu diacritice la nr de apariitii
							map.replace(secventa, inside_map1);
						}
						else// daca contine forma cu diacritice
						{
							Cuvant date_cuvant = map.get(secventa).get(cuvant_fara_diacritice).get(cuvinte[2]);
							date_cuvant.nr_aparitii += 1;
							inside_map2 = map.get(secventa).get(cuvant_fara_diacritice);
							inside_map2.replace(cuvinte[2], date_cuvant);
							
							
							//mapez cuvantul cu fara diacritice la hashmapul care mapeaza forma cu diacritice la nr de aparitii
							inside_map1.replace(cuvant_fara_diacritice, inside_map2);
							
							//mapez secventa de doua cuvinte cu diacritice la hashmapul care mapeaza cuvantul fara diacritice la hashmapul care mapeaza formele cu diacritice la nr de apariitii
							map.replace(secventa, inside_map1);
						}
					}
				}
			}
			
			
			
			
			
			
			
			for (int i = 3; i < cuvinte.length ; i++)
			{
				secventa = cuvinte[i - 2] + " " + cuvinte[i - 1];
				cuvant_fara_diacritice = secventaFaraDiacritice(cuvinte[i]);
				if(map.containsKey(secventa) == false)
				{
					inside_map1 = new HashMap<>();
					inside_map2 = new HashMap<>();
					
					//mapez cuvantul cu diacritice la nr_de apartii
					Cuvant date_cuvant = new Cuvant(1);
					inside_map2.put(cuvinte[i], date_cuvant);
					
					//mapez cuvantul cu fara diacritice la hashmapul care mapeaza forma cu diacritice la nr de aparitii
					inside_map1.put(cuvant_fara_diacritice, inside_map2);
					
					//mapez secventa de doua cuvinte cu diacritice la hashmapul care mapeaza cuvantul fara diacritice la hashmapul care mapeaza formele cu diacritice la nr de apariitii
					map.put(secventa, inside_map1);
				}
				else // contine secventa cu diacritice
				{
					if(map.get(secventa).containsKey(cuvant_fara_diacritice) == false)//nu a mai intalnit niciodata nicio forma a acelui cuvant
					{
						inside_map2 = new HashMap<>();
						//mapez cuvantul cu diacritice la nr_de apartii
						Cuvant date_cuvant = new Cuvant(1);
						inside_map2.put(cuvinte[i], date_cuvant);
						
						//mapez cuvantul cu fara diacritice la hashmapul care mapeaza forma cu diacritice la nr de aparitii
						inside_map1 = (HashMap<String, HashMap<String, Cuvant>>) map.get(secventa);
						inside_map1.put(cuvant_fara_diacritice, inside_map2);
						
						//mapez secventa de doua cuvinte cu diacritice la hashmapul care mapeaza cuvantul fara diacritice la hashmapul care mapeaza formele cu diacritice la nr de apariitii
						map.replace(secventa, inside_map1);
					}
					
					//AICI AM RAMAS IERI
					
					else//s-au mai intalnit forme ale acestui cuvant
					{
						if(map.get(secventa).get(cuvant_fara_diacritice).containsKey(cuvinte[i]) == false)//daca nu contine forma cu diacritice
						{
							//mapez cuvantul cu diacritice la nr_de apartii
							Cuvant date_cuvant = new Cuvant(1);
							inside_map2 = map.get(secventa).get(cuvant_fara_diacritice);
							inside_map2.put(cuvinte[i], date_cuvant);
							
							//mapez cuvantul cu fara diacritice la hashmapul care mapeaza forma cu diacritice la nr de aparitii
							inside_map1 = map.get(secventa);
							inside_map1.replace(cuvant_fara_diacritice, inside_map2);
							
							//mapez secventa de doua cuvinte cu diacritice la hashmapul care mapeaza cuvantul fara diacritice la hashmapul care mapeaza formele cu diacritice la nr de apariitii
							map.replace(secventa, inside_map1);
						}
						else// daca contine forma cu diacritice
						{
							Cuvant date_cuvant = map.get(secventa).get(cuvant_fara_diacritice).get(cuvinte[i]);
							date_cuvant.nr_aparitii += 1;
							inside_map2 = map.get(secventa).get(cuvant_fara_diacritice);
							inside_map2.replace(cuvinte[i], date_cuvant);
							
							
							//mapez cuvantul cu fara diacritice la hashmapul care mapeaza forma cu diacritice la nr de aparitii
							inside_map1.replace(cuvant_fara_diacritice, inside_map2);
							
							//mapez secventa de doua cuvinte cu diacritice la hashmapul care mapeaza cuvantul fara diacritice la hashmapul care mapeaza formele cu diacritice la nr de apariitii
							map.replace(secventa, inside_map1);
						}
					}
				}
			}
		}
		
    	}
		
		
		
		
		
		
		//iterez prin ele si printez hashmap-ul
/*

		for (Map.Entry<String, HashMap<String, HashMap<String, Cuvant>>> entry1 : map.entrySet()) {
			 secventa_cu_diacritice = entry1.getKey();

			for (Map.Entry<String, HashMap<String, Cuvant>> entry2 : entry1.getValue().entrySet()) {
				String cuvant_fara_diacritice = entry2.getKey();
				
					for (Map.Entry<String, Cuvant> entry3 : entry2.getValue().entrySet()) {
						String cuvant_cu_diacritice = entry3.getKey();
						Cuvant date_cuvant = entry3.getValue();
						System.out.println(secventa_cu_diacritice + ": " + cuvant_fara_diacritice + ": " + cuvant_cu_diacritice + ": " + date_cuvant.nr_aparitii);
						// ...
					}
			}
		}
	*/	
		
		
		
		//DECODARE
		
		File input_file = new File("/home/bala/Desktop/DiacriticsChalange/diacritics-restoration-corpus/talkshows.dev_v7.wodia");
		PrintWriter writer = new PrintWriter("/home/bala/Desktop/DiacriticsChalange/diacritics-restoration-corpus/Bala_Dev");
		Scanner scan = new Scanner(input_file);
		String linie;
		String[] cuvinte;
		while (scan.hasNextLine())
		{
			
			
			
		//DECODAREA PRIMELOR DOUA CUVINTE DIN FIECARE LINIE CU UN UNIGRAM	
			linie = scan.nextLine();
			cuvinte = linie.split(" ");
			String line_with_diacritics = "";
			if(cuvinte.length >= 2)
			{
				for (int i = 0; i < 2; i++)
				{
					String cuv_max = "";
					int max = 0;
					if(map1.containsKey(cuvinte[i]))
					{
						inside_map = map1.get(cuvinte[i]);
						for (Map.Entry<String, Cuvant> entry : inside_map.entrySet()) {
							String cuvant_cu_diacritice = entry.getKey();
							Cuvant date_cuvant = entry.getValue();
							if(date_cuvant.nr_aparitii > max)
							{
								max = date_cuvant.nr_aparitii;
								cuv_max = cuvant_cu_diacritice;
							}
						}
						line_with_diacritics += cuv_max + " ";
					}
					else
					{
						line_with_diacritics += cuvinte[i] + " ";
					}
				}
			}
			else
			{
				for (int i = 0; i < cuvinte.length; i++)
				{
					String cuv_max = "";
					int max = 0;
					if(map1.containsKey(cuvinte[i]))
					{
						inside_map = map1.get(cuvinte[i]);
						for (Map.Entry<String, Cuvant> entry : inside_map.entrySet()) {
							String cuvant_cu_diacritice = entry.getKey();
							Cuvant date_cuvant = entry.getValue();
							if(date_cuvant.nr_aparitii > max)
							{
								max = date_cuvant.nr_aparitii;
								cuv_max = cuvant_cu_diacritice;
							}
						}
						line_with_diacritics += cuv_max + " ";
					}
					else
					{
						line_with_diacritics += cuvinte[i] + " ";
					}
				}
			}
			
			
			
			
			
			//DECODARE CU TRIGRAM
			
			for (int i = 2; i < cuvinte.length; i++)
			{
				String secventa_anterioara = cuvinte[i - 2] + " " + cuvinte[i - 1];
				String cuv_max = "";
				int max = 0;
				if(map.containsKey(secventa_anterioara))//contine secventa asa ca verific daca cintine si cuvantul curent
				{
					if(map.get(secventa_anterioara).containsKey(cuvinte[i]))// s-a mai intalnit cuvantul curent dupa secventa de doua cuvinte
					{
						inside_map2 = map.get(secventa_anterioara).get(cuvinte[i]);
						for (Map.Entry<String, Cuvant> entry : inside_map2.entrySet()) {
							String cuvant_cu_diacritice = entry.getKey();
							Cuvant date_cuvant = entry.getValue();
							if(date_cuvant.nr_aparitii > max)
							{
								max = date_cuvant.nr_aparitii;
								cuv_max = cuvant_cu_diacritice;
							}
						}
						line_with_diacritics += cuv_max + " ";
					}
					else// nu s-a mai intalnit cuvantul curent dupa secventa anterioara de doua cuvinte
					{
						if(map1.containsKey(cuvinte[i]))
						{
							inside_map = map1.get(cuvinte[i]);
							for (Map.Entry<String, Cuvant> entry : inside_map.entrySet()) {
								String cuvant_cu_diacritice = entry.getKey();
								Cuvant date_cuvant = entry.getValue();
								if(date_cuvant.nr_aparitii > max)
								{
									max = date_cuvant.nr_aparitii;
									cuv_max = cuvant_cu_diacritice;
								}
							}
							line_with_diacritics += cuv_max + " ";
						}
						else
						{
							line_with_diacritics += cuvinte[i] + " ";
						}
					}
				}
				else // nu contine secventa asa ca folosesc unigram
				{
					if(map1.containsKey(cuvinte[i]))
					{
						inside_map = map1.get(cuvinte[i]);
						for (Map.Entry<String, Cuvant> entry : inside_map.entrySet()) {
							String cuvant_cu_diacritice = entry.getKey();
							Cuvant date_cuvant = entry.getValue();
							if(date_cuvant.nr_aparitii > max)
							{
								max = date_cuvant.nr_aparitii;
								cuv_max = cuvant_cu_diacritice;
							}
						}
						line_with_diacritics += cuv_max + " ";
					}
					else
					{
						line_with_diacritics += cuvinte[i] + " ";
					}
				}
			}
			
			
			
			writer.println(line_with_diacritics);
		}
		
		
	}
	public static String secventaFaraDiacritice(String secventa)
	{
		String secventa_fara_diacritice;
		char[] secventa_char;
		secventa_char = secventa.toCharArray();
		for (int j = 0; j < secventa_char.length; j++)
		{
			switch (secventa_char[j])
			{
				case 'ă':
					secventa_char[j] = 'a';
					break;
				case 'â':
					secventa_char[j] = 'a';
					break;
				case 'î':
					secventa_char[j] = 'i';
					break;
				case 'ş':
					secventa_char[j] = 's';
					break;
				case 'ţ':
					secventa_char[j] = 't';
					break;
			}
		}
		secventa_fara_diacritice = new String(secventa_char);
		return secventa_fara_diacritice;
	}

}
